**Smart Doctor**


## Introdução 
APP desenvolvido em PHP.

Requisitos

XAMPP

**Executar projeto**

1. Abra o XAMPP
2. Inicie o Apache e o MySQL;
3. Faça o download do projeto 
4. Crie uma pasta com o nome "smartdoctor" e coloque-o na pasta 
5. Importar o arquivo "smartdoctorbd" para o banco de dados (http://localhost/phpmyadmin)
6. Abra a o projeto pelo navegador através do link: http://127.0.0.1/smartdoctor/index.html
7. Para logar no sistema, será disponibilizadas as contas de usuários abaixo

**Conta de Administrador**

login: luiz@smartdoctor.com
senha: 123456

**Conta de Paciente**

login: luizpaciente@hotmail.com
senha: 123456

**Conta de Médico**

login: drjessica@smartdoctor.com
senha: 123456

**Conta de Funcionário**

login: julia@smartdoctor.com
senha: 123456

Autor - Luiz Buchvaitz
